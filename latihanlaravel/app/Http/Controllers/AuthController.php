<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('page.form');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $depan = $request['namadepan'];
        $belakang = $request['namabelakang'];

        return view('page.welcome', compact('depan', 'belakang'));
    }
}
