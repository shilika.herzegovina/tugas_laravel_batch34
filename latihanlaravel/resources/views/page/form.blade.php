@extends('layout.master') 
@section('title')
<h1>Buat Account Baru!</h1>
@endsection 
@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label>First Name</label> <br><br>
    <input type="text" name="namadepan"> <br> <br>
    <label>Last Name</label>  <br><br>
    <input type="text" name="namabelakang"> <br> <br>
    <label>Gender:</label>  <br><br>
    <input type="radio" name="gender" >Male <br>
    <input type="radio" name="gender" >Female <br>
    <input type="radio" name="gender" >Other <br><br>
    <label>Nationality</label> <br><br>
    <select name="NT">
        <option value="1">Indonesian</option>
        <option value="1">Australian</option>
        <option value="1">USA</option>
        <option value="1">Canadian</option>
        <option value="1">Chinese</option>
        <option value="1">British</option>
    </select> <br><br>
    <label>Language Spoken:</label>  <br><br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br> <br>
    <label>Bio:</label> <br>
    <textarea name="Biodata" cols="30" rows="10"></textarea> <br><br>
    <input type="submit">
</form> 
@endsection